add_subdirectory(geometry)
add_subdirectory(topology)
add_subdirectory(qsm)

target_include_directories(SFL PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
