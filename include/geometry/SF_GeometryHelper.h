/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#pragma once

#include <Eigen/Dense>
#include <numbers>

#include "SF_Geometry.h"

namespace sf {
namespace geometry {
namespace helper {

constexpr double MAX_ERROR = 0.000'000'000'000'000'1;
constexpr double RAD_TO_DEG = 180. / std::numbers::pi;
constexpr double DEG_TO_RAD = std::numbers::pi / 180.;

std::shared_ptr<SF_Geometry>
Clone(const std::shared_ptr<SF_Geometry>& geometry);
double
DistanceBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second);
double
AngleInRadBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second);
double
AngleInDegBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second);
double
RadToDeg(double radian);
double
DegToRad(double degree);

} // namespace helper
} // namespace geometry

} // namespace sf
