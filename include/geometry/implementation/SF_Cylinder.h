/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

 *****************************************************************************/

#pragma once

#include "SF_Geometry3d.h"
#include "SF_LineSegment.h"

namespace sf {
namespace geometry {

class SF_Cylinder
  : public SF_LineSegment
  , public SF_Geometry3d
{
public:
  SF_Cylinder(const SF_LineSegment& segment, double radius);
  SF_Cylinder(const Eigen::Vector3d& start,
              const Eigen::Vector3d& end,
              double radius);
  void Serialize(std::ostream& ostream) const override;
  void SerializeHeader(std::ostream& ostream) const override;
  double SqrdDistanceTo(const Eigen::Vector3d& point) const override;
  void Transform(const Eigen::Affine3d& transform) override;
  double DistanceTo(const Eigen::Vector3d& point) const override;
  double Volume() const override;
  double Surface() const override;
  double Radius() const override;
  void SetRadius(double radius) override;
  double Length() const override;
  double Area() const override;
  double BoundingRadius() const override;
  Eigen::Vector3d Center() const override;
  GEOMETRY_TYPE GeometryType() const override;

private:
  bool IsEqual(const SF_Geometry& other) const override;
  double m_radius;
  double m_volume;
  double m_surface;
  double m_area;
};

} // namespace geometry
} // namespace sf
