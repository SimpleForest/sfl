/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#pragma once

#include "SF_Struct.h"
#include <Eigen/Dense>

namespace sf {
namespace geometry {

class SF_Geometry : public virtual SF_Struct
{
public:
  enum class GEOMETRY_TYPE
  {
    CYLINDER,
    LINE,
    SEGMENT
  };

public:
  virtual void Transform(const Eigen::Affine3d& transform) = 0;
  virtual double DistanceTo(const Eigen::Vector3d& point) const = 0;
  virtual double SqrdDistanceTo(const Eigen::Vector3d& point) const = 0;
  virtual GEOMETRY_TYPE GeometryType() const = 0;
  bool operator==(const SF_Geometry& other) const;
  bool operator!=(const SF_Geometry& other) const;

protected:
  virtual bool IsEqual(const SF_Geometry& other) const = 0;
};

} // namespace geometry
} // namespace sf
