/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#pragma once

#include "SF_Struct.h"

namespace sf {
namespace qsm {

class SF_AllometricParameters : SF_Struct
{
public:
  void Serialize(std::ostream& ostream) const override;
  void SerializeHeader(std::ostream& ostream) const override;

  double m_growthVolume = 0.;
  double m_growthLength = 0.;
  std::int32_t m_reverseBranchOrder = 0;
  std::int32_t m_branchOrder = 0;
  std::int32_t m_reverseBranchOrderPipeArea = 0;
  double m_reverseBranchOrderPipeRadius = 0.;
  double m_proxyVolume = 0.;
  double m_vesselVolume = 0.;

private:
  double m_volume = 0.;
  double m_length = 0.;
};

} // namespace qsm
} // namespace sf
