/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#pragma once

#include "SF_AllometricParameters.h"

#include "SF_Geometry3d.h"
#include "SF_Node.h"

namespace sf {
namespace qsm {

class SF_BuildingBrick : public topology::SF_Node
{
public:
  SF_BuildingBrick(std::shared_ptr<geometry::SF_Geometry3d> geometry,
                   std::uint32_t id);

  std::shared_ptr<geometry::SF_Geometry3d> Geometry() const;
  void SetGeometry(std::shared_ptr<geometry::SF_Geometry3d> geometry);

  void Serialize(std::ostream& ostream) const override;
  void SerializeHeader(std::ostream& ostream) const override;
  SF_BuildingBrick& operator+=(const std::shared_ptr<SF_BuildingBrick>& other);
  SF_AllometricParameters Parameters() const;
  void SetParameters(const SF_AllometricParameters& params);

private:
  std::shared_ptr<SF_Node> Copy() const override;
  bool IsEqual(const SF_Node& other) const override;
  qsm::SF_AllometricParameters m_parameters;
  std::shared_ptr<geometry::SF_Geometry3d> m_geometry;
};

} // namespace qsm
} // namespace sf
