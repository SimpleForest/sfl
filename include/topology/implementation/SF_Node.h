/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#pragma once

#include <vector>

#include "SF_Struct.h"

namespace sf {
namespace topology {

class SF_Node
  : public SF_Struct
  , public std::enable_shared_from_this<SF_Node>
{
public:
  SF_Node(const std::uint32_t id);
  void Serialize(std::ostream& ostream) const override;
  void SerializeHeader(std::ostream& ostream) const override;

  std::vector<std::shared_ptr<SF_Node>> ChildrenRecursively();
  std::shared_ptr<SF_Node> DeepCopy() const;

  SF_Node& operator+=(const std::shared_ptr<SF_Node>& other);
  bool operator==(const SF_Node& other) const;
  bool operator!=(const SF_Node& other) const;

protected:
  virtual std::shared_ptr<SF_Node> Copy() const;
  virtual bool IsEqual(const SF_Node& other) const;
  virtual void AddChild(const std::shared_ptr<SF_Node>& other);

private:
  std::vector<std::shared_ptr<SF_Node>> m_children;
  std::weak_ptr<SF_Node> m_parent;
  std::int32_t m_id;

public:
  std::int32_t ParentId() const;

  std::int32_t Id() const;
  void SetId(const int32_t id);

  std::shared_ptr<SF_Node> Parent() const;
  void SetParent(const std::shared_ptr<SF_Node>& parent);

  std::vector<std::shared_ptr<SF_Node>> Children() const;
  void SetChildren(const std::vector<std::shared_ptr<SF_Node>>& children);
};

} // namespace topology
} // namespace sf
