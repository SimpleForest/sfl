#include "SF_Header.h"

namespace sf {

SF_Header::SF_Header(const SF_Struct& sfStruct)
  : m_struct(sfStruct)
{
}

void
SF_Header::Serialize(std::ostream& ostream) const
{
  m_struct.SerializeHeader(ostream);
}

} // namespace sf
