#include "SF_GeometryHelper.h"

#include "SF_Cylinder.h"
#include "SF_Line.h"
#include "SF_LineSegment.h"

namespace sf {
namespace geometry {
namespace helper {

std::shared_ptr<SF_Geometry>
Clone(const std::shared_ptr<SF_Geometry>& geometry)
{
  switch (geometry->GeometryType()) {
    case SF_Geometry::GEOMETRY_TYPE::CYLINDER:
      return std::make_shared<SF_Cylinder>(
        dynamic_cast<SF_Cylinder&>(*geometry));
    case SF_Geometry::GEOMETRY_TYPE::SEGMENT:
      return std::make_shared<SF_LineSegment>(
        dynamic_cast<SF_LineSegment&>(*geometry));
    case SF_Geometry::GEOMETRY_TYPE::LINE:
      return std::make_shared<SF_Line>(dynamic_cast<SF_Line&>(*geometry));
    default:
      throw std::invalid_argument(
        "The geometry type to be copied is not supported.");
      break;
  }
}

double
DistanceBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second)
{
  return (second - first).norm();
}

double
AngleInRadBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second)
{
  if (first == second) {
    return 0;
  }
  return std::acos((first.dot(second)) / (first.norm() * second.norm()));
}

double
RadToDeg(double radian)
{
  return radian * RAD_TO_DEG;
}

double
DegToRad(double degree)
{
  return degree * DEG_TO_RAD;
}

double
AngleInDegBetween(const Eigen::Vector3d& first, const Eigen::Vector3d& second)
{
  return RadToDeg(AngleInRadBetween(first, second));
}

} // namespace helper
} // namespace geometry
} // namespace sf
