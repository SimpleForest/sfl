#include "SF_Cylinder.h"

#include <iomanip>
#include <numbers>

#include "SF_GeometryHelper.h"

namespace sf {
namespace geometry {
SF_Cylinder::SF_Cylinder(const SF_LineSegment& segment, double radius)
  : SF_LineSegment(segment)
  , m_radius(radius)
{
  if (m_radius <= 0) {
    throw std::invalid_argument("Radius must be postive.");
  }
  const auto halfLength = SF_LineSegment::BoundingRadius();
  m_boundingRadius = std::sqrt(halfLength * halfLength + m_radius * m_radius);
  m_volume = std::numbers::pi * m_radius * m_radius * m_length;
  m_surface = std::numbers::pi * m_radius * 2 * m_length;
  m_area = m_radius * m_length * 2;
}

SF_Cylinder::SF_Cylinder(const Eigen::Vector3d& start,
                         const Eigen::Vector3d& end,
                         double radius)
  : SF_Cylinder({ start, end }, radius)
{
}

double
SF_Cylinder::SqrdDistanceTo(const Eigen::Vector3d& point) const
{
  const auto projection = SF_Line::Projection(point);
  const auto distanceOfProjection = DistanceOfProjection(projection);
  const auto distanceToProjection =
    std::abs(helper::DistanceBetween(point, projection) - Radius());
  return distanceOfProjection * distanceOfProjection +
         distanceToProjection * distanceToProjection;
}

void
SF_Cylinder::Transform(const Eigen::Affine3d& transform)
{
  *this = SF_Cylinder(transform * m_start, transform * m_end, m_radius);
}

double
SF_Cylinder::DistanceTo(const Eigen::Vector3d& point) const
{
  return SF_LineSegment::DistanceTo(point);
}

void
SF_Cylinder::Serialize(std::ostream& ostream) const
{
  ostream << std::fixed << std::setprecision(4);
  SF_LineSegment::Serialize(ostream);
  ostream << ", " << m_radius;
}

void
SF_Cylinder::SerializeHeader(std::ostream& ostream) const
{
  SF_LineSegment::SerializeHeader(ostream);
  ostream << ", radius";
}

double
SF_Cylinder::Volume() const
{
  return m_volume;
}

double
SF_Cylinder::Surface() const
{
  return m_surface;
}

double
SF_Cylinder::Radius() const
{
  return m_radius;
}

void
SF_Cylinder::SetRadius(double radius)
{
  m_radius = radius;
}

double
SF_Cylinder::Length() const
{
  return m_length;
}

double
SF_Cylinder::Area() const
{
  return m_area;
}

double
SF_Cylinder::BoundingRadius() const
{
  return m_boundingRadius;
}

Eigen::Vector3d
SF_Cylinder::Center() const
{
  return m_center;
}

SF_Geometry::GEOMETRY_TYPE
SF_Cylinder::GeometryType() const
{
  return GEOMETRY_TYPE::CYLINDER;
}

bool
SF_Cylinder::IsEqual(const SF_Geometry& other) const
{
  const auto otherCylinder = dynamic_cast<const SF_Cylinder&>(other);
  return SF_LineSegment::IsEqual(otherCylinder) &&
         m_radius == otherCylinder.m_radius;
}

} // namespace geometry
} // namespace sf
