#include "SF_Line.h"

#include <iomanip>
#include <iostream>

#include "SF_GeometryHelper.h"
#include "SF_Helper.h"

namespace sf {
namespace geometry {

SF_Line::SF_Line(const Eigen::Vector3d& first, const Eigen::Vector3d& second)
  : m_first(first)
  , m_second(second)
{
  m_axis = (m_second - m_first).normalized();
}

double
SF_Line::DistanceTo(const Eigen::Vector3d& point) const
{
  return helper::DistanceBetween(point, Projection(point));
}

double
SF_Line::SqrdDistanceTo(const Eigen::Vector3d& point) const
{
  const auto dist = DistanceTo(point);
  return dist * dist;
}

double
SF_Line::Length() const
{
  return std::numeric_limits<double>::infinity();
}

double
SF_Line::BoundingRadius() const
{
  return std::numeric_limits<double>::infinity();
}

Eigen::Vector3d
SF_Line::Center() const
{
  return Eigen::Vector3d(std::numeric_limits<double>::quiet_NaN(),
                         std::numeric_limits<double>::quiet_NaN(),
                         std::numeric_limits<double>::quiet_NaN());
}

SF_Geometry::GEOMETRY_TYPE
SF_Line::GeometryType() const
{
  return GEOMETRY_TYPE::LINE;
}

void
SF_Line::Transform(const Eigen::Affine3d& transform)
{
  *this = SF_Line(transform * m_first, transform * m_second);
}

void
SF_Line::Serialize(std::ostream& ostream) const
{
  ostream << std::fixed << std::setprecision(4);
  ostream << m_first.x() << sf::SF_SEPERATOR << m_first.y() << sf::SF_SEPERATOR
          << m_first.z() << sf::SF_SEPERATOR << m_second.x() << sf::SF_SEPERATOR
          << m_second.y() << sf::SF_SEPERATOR << m_second.z();
}

void
SF_Line::SerializeHeader(std::ostream& ostream) const
{
  ostream << "firstX" << sf::SF_SEPERATOR << "firstY" << sf::SF_SEPERATOR
          << "firstZ" << sf::SF_SEPERATOR << "secondX" << sf::SF_SEPERATOR
          << "secondY" << sf::SF_SEPERATOR << "secondZ";
}

Eigen::Vector3d
SF_Line::Projection(const Eigen::Vector3d& point) const
{
  const auto a = point - m_first;
  const auto& b = m_axis;
  return m_first + (a.dot(b)) * b;
}

bool
SF_Line::IsEqual(const SF_Geometry& other) const
{
  return std::tie(m_first, m_second) == std::tie(m_first, m_second);
}

} // namespace geometry
} // namespace sf
