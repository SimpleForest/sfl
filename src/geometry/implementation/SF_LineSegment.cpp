#include "SF_LineSegment.h"

#include "SF_GeometryHelper.h"
#include "SF_Helper.h"

#include <Eigen/Geometry>
#include <iomanip>

namespace sf {
namespace geometry {

SF_LineSegment::SF_LineSegment(const Eigen::Vector3d& start,
                               const Eigen::Vector3d& end)
  : SF_Line(start, end)
  , m_start(start)
  , m_end(end)
{
  m_center = (m_start + m_end) / 2.;
  m_length = helper::DistanceBetween(m_start, m_end);
  m_boundingRadius = m_length / 2.;
}

double
SF_LineSegment::Length() const
{
  return m_length;
}

Eigen::Vector3d
SF_LineSegment::Center() const
{
  return m_center;
}

SF_Geometry::GEOMETRY_TYPE
SF_LineSegment::GeometryType() const
{
  return GEOMETRY_TYPE::SEGMENT;
}

double
SF_LineSegment::DistanceTo(const Eigen::Vector3d& point) const
{
  return std::sqrt(SqrdDistanceTo(point));
}

double
SF_LineSegment::SqrdDistanceTo(const Eigen::Vector3d& point) const
{
  const auto projection = SF_Line::Projection(point);
  const auto distanceOfProjection = DistanceOfProjection(projection);
  const auto distanceToProjection =
    std::abs(helper::DistanceBetween(point, projection));
  return distanceOfProjection * distanceOfProjection +
         distanceToProjection * distanceToProjection;
}

void
SF_LineSegment::Transform(const Eigen::Affine3d& transform)
{
  *this = SF_LineSegment(transform * m_start, transform * m_end);
}

void
SF_LineSegment::Serialize(std::ostream& ostream) const
{
  ostream << std::fixed << std::setprecision(4);
  ostream << m_start.x() << sf::SF_SEPERATOR << m_start.y() << sf::SF_SEPERATOR
          << m_start.z() << sf::SF_SEPERATOR << m_end.x() << sf::SF_SEPERATOR
          << m_end.y() << sf::SF_SEPERATOR << m_end.z();
}

void
SF_LineSegment::SerializeHeader(std::ostream& ostream) const
{
  ostream << "startX" << sf::SF_SEPERATOR << "startY" << sf::SF_SEPERATOR
          << "startZ" << sf::SF_SEPERATOR << "endX" << sf::SF_SEPERATOR
          << "endY" << sf::SF_SEPERATOR << "endZ";
}

double
SF_LineSegment::BoundingRadius() const
{
  return m_boundingRadius;
}

double
SF_LineSegment::DistanceOfProjection(const Eigen::Vector3d& projection) const
{
  const auto distToStart = helper::DistanceBetween(m_start, projection);
  const auto distToEnd = helper::DistanceBetween(m_end, projection);
  return (distToStart <= m_length && distToEnd <= m_length)
           ? 0.
           : std::min(distToStart, distToEnd);
}
} // namespace geometry
} // namespace sf
