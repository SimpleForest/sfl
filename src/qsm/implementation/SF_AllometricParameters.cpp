/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "SF_AllometricParameters.h"
#include "SF_Helper.h"

#include <iomanip>

namespace sf {
namespace qsm {

void
SF_AllometricParameters::Serialize(std::ostream& ostream) const
{
  ostream << std::fixed << std::setprecision(4);
  ostream << m_growthVolume << sf::SF_SEPERATOR << m_growthLength
          << sf::SF_SEPERATOR << m_reverseBranchOrder << sf::SF_SEPERATOR
          << m_branchOrder << sf::SF_SEPERATOR << m_reverseBranchOrderPipeArea
          << sf::SF_SEPERATOR << m_reverseBranchOrderPipeRadius
          << sf::SF_SEPERATOR << m_proxyVolume << sf::SF_SEPERATOR
          << m_vesselVolume;
}

void
SF_AllometricParameters::SerializeHeader(std::ostream& ostream) const
{
  ostream << "growthVolume" << sf::SF_SEPERATOR << "growthLength"
          << sf::SF_SEPERATOR << "reverseBranchOrder" << sf::SF_SEPERATOR
          << "branchOrder" << sf::SF_SEPERATOR << "reverseBranchOrderPipeArea"
          << sf::SF_SEPERATOR << "reverseBranchOrderPipeRadius"
          << sf::SF_SEPERATOR << "proxyVolume" << sf::SF_SEPERATOR
          << "vesselVolume";
}
}
}
