/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "SF_BuildingBrick.h"

#include "SF_GeometryHelper.h"
#include "SF_Helper.h"

namespace sf {
namespace qsm {

SF_BuildingBrick::SF_BuildingBrick(
  std::shared_ptr<geometry::SF_Geometry3d> geometry,
  const uint32_t id)
  : SF_Node(id)
  , m_geometry(geometry)
{
}

void
SF_BuildingBrick::Serialize(std::ostream& ostream) const
{
  const auto geometry = Geometry();
  SF_Node::Serialize(ostream);
  if (geometry) {
    ostream << SF_SEPERATOR;
    geometry->Serialize(ostream);
  }
}

void
SF_BuildingBrick::SerializeHeader(std::ostream& ostream) const
{
  const auto geometry = Geometry();
  SF_Node::SerializeHeader(ostream);
  if (geometry) {
    ostream << SF_SEPERATOR;
    geometry->SerializeHeader(ostream);
  }
}

SF_BuildingBrick&
SF_BuildingBrick::operator+=(const std::shared_ptr<SF_BuildingBrick>& other)
{
  return dynamic_cast<SF_BuildingBrick&>(topology::SF_Node::operator+=(
    std::static_pointer_cast<topology::SF_Node>(other)));
}

std::shared_ptr<topology::SF_Node>
SF_BuildingBrick::Copy() const
{
  auto geometryCopy = geometry::helper::Clone(Geometry());
  auto brickCopy = std::make_shared<SF_BuildingBrick>(
    std::dynamic_pointer_cast<geometry::SF_Geometry3d>(geometryCopy), Id());
  return std::static_pointer_cast<topology::SF_Node>(brickCopy);
}

SF_AllometricParameters
SF_BuildingBrick::Parameters() const
{
  return m_parameters;
}

void
SF_BuildingBrick::SetParameters(const SF_AllometricParameters& params)
{
  m_parameters = params;
}

bool
SF_BuildingBrick::IsEqual(const SF_Node& other) const
{
  auto otherBrick = dynamic_cast<const SF_BuildingBrick&>(other);
  if (*Geometry() != *otherBrick.Geometry()) {
    return false;
  }
  return SF_Node::IsEqual(other);
}

std::shared_ptr<geometry::SF_Geometry3d>
SF_BuildingBrick::Geometry() const
{
  return m_geometry;
}

void
SF_BuildingBrick::SetGeometry(std::shared_ptr<geometry::SF_Geometry3d> geometry)
{
  m_geometry = std::move(geometry);
}

} // namespace qsm
} // namespace sf
