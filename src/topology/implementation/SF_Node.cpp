/****************************************************************************

Copyright (C) 2017-2023 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForestLibrary
Successor of SimpleTree and SimpleForest plugins in Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "SF_Node.h"

#include <ranges>
#include <tuple>

#include <array>
#include <iostream>
#include <list>
#include <ranges>
#include <string>
#include <tuple>
#include <vector>

namespace sf {
namespace topology {

SF_Node::SF_Node(const uint32_t id)
  : m_id(static_cast<int32_t>(id))
{
}

void
SF_Node::Serialize(std::ostream& ostream) const
{
  ostream << Id() << ", " << ParentId();
}

void
SF_Node::SerializeHeader(std::ostream& ostream) const
{
  ostream << "Id, ParentId";
}

void
SF_Node::SetParent(const std::shared_ptr<SF_Node>& parent)
{
  m_parent = parent;
}

std::shared_ptr<SF_Node>
SF_Node::Parent() const
{
  return m_parent.lock();
}

std::vector<std::shared_ptr<SF_Node>>
SF_Node::Children() const
{
  return m_children;
}

std::vector<std::shared_ptr<SF_Node>>
SF_Node::ChildrenRecursively()
{
  std::vector<std::shared_ptr<SF_Node>> children;
  for (auto& child : m_children) {
    const auto& grandChildren = child->ChildrenRecursively();
    children.insert(children.end(),
                    std::make_move_iterator(grandChildren.begin()),
                    std::make_move_iterator(grandChildren.end()));
  }
  children.push_back(shared_from_this());
  return children;
}

std::shared_ptr<SF_Node>
SF_Node::DeepCopy() const
{
  const auto nodeCopy = Copy();
  for (const auto& child : m_children) {
    auto childCopy = child->DeepCopy();
    (*nodeCopy) += childCopy;
  }
  return nodeCopy;
}

std::shared_ptr<SF_Node>
SF_Node::Copy() const
{
  return std::make_shared<SF_Node>(m_id);
}

int32_t
SF_Node::Id() const
{
  return m_id;
}

int32_t
SF_Node::ParentId() const
{
  const auto& parent = Parent();
  if (!parent) {
    return -1;
  }
  return parent->Id();
}

void
SF_Node::SetId(const int32_t id)
{
  m_id = id;
}

SF_Node&
SF_Node::operator+=(const std::shared_ptr<SF_Node>& other)
{
  if (typeid(*this) == typeid(*other)) {
    AddChild(other);
  }
  return *this;
}

bool
SF_Node::operator==(const SF_Node& other) const
{
  if (typeid(*this) != typeid(other)) {
    return false;
  }
  return IsEqual(other);
}

bool
SF_Node::operator!=(const SF_Node& other) const
{
  return !(SF_Node::operator==(other));
}

bool
SF_Node::IsEqual(const SF_Node& other) const
{
  const auto hasEqualIds = m_id == other.m_id;
  const auto hasEqualNumberChildren =
    m_children.size() == other.m_children.size();
  auto result = hasEqualIds && hasEqualNumberChildren;
  for (const auto& [child, otherChild] :
       std::views::zip(m_children, other.m_children)) {
    result &= *child == *otherChild;
  }
  return result;
}

void
SF_Node::AddChild(const std::shared_ptr<SF_Node>& other)
{
  other->SetParent(shared_from_this());
  m_children.push_back(other);
}

void
SF_Node::SetChildren(const std::vector<std::shared_ptr<SF_Node>>& children)
{
  m_children = children;
}
}
}

////-------------------------------------------------------------------
//// Raw iterator with random access
////-------------------------------------------------------------------

// class blRawIterator
//{
// protected:
//   std::shared_ptr<sf::topology::SF_Node> m_ptr;

// public:
//   using iterator_category = std::random_access_iterator_tag;
//   using value_type = sf::topology::SF_Node;
//   using difference_type = std::ptrdiff_t;
//   using pointer = std::shared_ptr<sf::topology::SF_Node>;
//   using reference = sf::topology::SF_Node&;

// public:
//   blRawIterator(std::shared_ptr<sf::topology::SF_Node> ptr = nullptr)
//   {
//     m_ptr = ptr;
//   }
//   blRawIterator(const blRawIterator& rawIterator) = default;
//   ~blRawIterator() {}

//  blRawIterator& operator=(const blRawIterator& rawIterator) = default;
//  blRawIterator& operator=(std::shared_ptr<sf::topology::SF_Node> ptr)
//  {
//    m_ptr = ptr;
//    return (*this);
//  }

//  operator bool() const { return m_ptr ? true : false; }

//  bool operator==(const blRawIterator& rawIterator) const
//  {
//    return m_ptr == rawIterator.getConstPtr();
//  }
//  bool operator!=(const blRawIterator& rawIterator) const
//  {
//    return m_ptr != rawIterator.getConstPtr();
//  }

//  blRawIterator& operator+=(const difference_type& movement)
//  {
//    m_ptr += movement;
//    return (*this);
//  }
//  blRawIterator& operator-=(const difference_type& movement)
//  {
//    m_ptr -= movement;
//    return (*this);
//  }
//  blRawIterator& operator++()
//  {
//    ++m_ptr;
//    return (*this);
//  }
//  blRawIterator& operator--()
//  {
//    --m_ptr;
//    return (*this);
//  }
//  blRawIterator operator++(int)
//  {
//    auto temp(*this);
//    ++m_ptr;
//    return temp;
//  }
//  blRawIterator operator--(int)
//  {
//    auto temp(*this);
//    --m_ptr;
//    return temp;
//  }
//  blRawIterator operator+(const difference_type& movement)
//  {
//    auto oldPtr = m_ptr;
//    m_ptr += movement;
//    auto temp(*this);
//    m_ptr = oldPtr;
//    return temp;
//  }
//  blRawIterator operator-(const difference_type& movement)
//  {
//    auto oldPtr = m_ptr;
//    m_ptr -= movement;
//    auto temp(*this);
//    m_ptr = oldPtr;
//    return temp;
//  }

//  difference_type operator-(const blRawIterator& rawIterator)
//  {
//    return std::distance(rawIterator.getPtr(), this->getPtr());
//  }

//  sf::topology::SF_Node& operator*() { return *m_ptr; }
//  const sf::topology::SF_Node& operator*() const { return *m_ptr; }
//  std::shared_ptr<sf::topology::SF_Node> operator->() { return m_ptr; }

//  std::shared_ptr<sf::topology::SF_Node> getPtr() const { return m_ptr; }
//  const std::shared_ptr<sf::topology::SF_Node> getConstPtr() const
//  {
//    return m_ptr;
//  }
//};
