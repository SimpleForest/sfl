#include <gtest/gtest.h>

#include <numbers>

#include "SF_Cylinder.h"
#include "SF_GeometryHelper.h"

namespace sf {
namespace geometry {

TEST(CylinderTest, PointToCylinderDistanceIsCorrectX)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 1., 0., 0. }, 1.);
  const Eigen::Vector3d point(3., 0., 0.);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected = std::sqrt(5.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, PointToCylindertDistanceIsCorrectY)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 0., 1., 0. }, 2.);
  const Eigen::Vector3d point(0., -10., 0.);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected = std::sqrt(104.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, PointToCylinderDistanceIsCorrectZ)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 0., 0., 1. }, 3.);
  const Eigen::Vector3d point(0., 0., 0.5);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected = 3.;
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, PointToCylinderDistanceIsCorrectLeftOf)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 1., 0., 0. }, 1.);
  const Eigen::Vector3d point(-1., 2., 2.);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected =
    std::sqrt((std::sqrt(8.) - 1.) * (std::sqrt(8.) - 1.) + 1);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, PointToCylinderDistanceIsCorrectInBetween)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 1., 0., 0. }, 1.);
  const Eigen::Vector3d point(0.5, 1., 1.);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected = std::sqrt(2.) - 1.;
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, PointToCylinderDistanceIsCorrectRightOf)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 1., 0., 0. }, 4.);
  const Eigen::Vector3d point(3., 1., 1.);
  const auto distance = cylinder.DistanceTo(point);
  const auto expected =
    std::sqrt((4. - std::sqrt(2)) * (4. - std::sqrt(2)) + 4.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(CylinderTest, BoundingRadiusIsCorrect)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 3., 0., 0. }, 2.);
  const auto boundingRadius = cylinder.BoundingRadius();
  const auto expected = std::sqrt(1.5 * 1.5 + 4.);
  EXPECT_DOUBLE_EQ(expected, boundingRadius);
}

TEST(CylinderTest, VolumeIsCorrect)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 2., 0., 0. }, 3.);
  const auto volume = cylinder.Volume();
  const auto expected = 9. * std::numbers::pi * 2.;
  EXPECT_DOUBLE_EQ(expected, volume);
}

TEST(CylinderTest, VolumeIsCorrectIfNegative)
{
  const SF_Cylinder cylinder({ 0., 0., -1. }, { 0., 0., -3. }, 4.);
  const auto volume = cylinder.Volume();
  const auto expected = 16. * std::numbers::pi * 2.;
  EXPECT_DOUBLE_EQ(expected, volume);
}

TEST(CylinderTest, SurfaceIsCorrect)
{
  const SF_Cylinder cylinder({ 0., 0., 0. }, { 4., 0., 0. }, 3.);
  const auto surface = cylinder.Surface();
  const auto expected = 4. * 3. * std::numbers::pi * 2;
  EXPECT_DOUBLE_EQ(expected, surface);
}

TEST(CylinderTest, RotationPreservesLength)
{
  SF_Cylinder segment({ -1., -5., 17. }, { 3., 2., -13. }, 1.);
  const auto lengthBefore = segment.Length();
  Eigen::Affine3d rotation(Eigen::AngleAxis<double>(
    std::numbers::pi * 0.91, Eigen::Vector3d(5., 1., 3.).normalized()));
  segment.Transform(rotation);
  const auto lengthAfter = segment.Length();
  EXPECT_DOUBLE_EQ(lengthBefore, lengthAfter);
}

TEST(CylinderTest, RotationPreservesRadius)
{
  SF_Cylinder segment({ -1., -5., 17. }, { 3., 2., -13. }, 1.);
  const auto radiusBefore = segment.Radius();
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    std::numbers::pi * 0.91, Eigen::Vector3d(5., 1., 3.).normalized()));
  segment.Transform(rotationAroundY90);
  const auto radiusAfter = segment.Radius();
  EXPECT_DOUBLE_EQ(radiusBefore, radiusAfter);
}

TEST(CylinderTest, RotationModifiesCenterY1)
{
  SF_Cylinder segment({ 0., 0., 0. }, { 0., 0., 1. }, 1.);
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    std::numbers::pi / 2., Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0.5, 0., -0. };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(CylinderTest, RotationModifiesCenterY2)
{
  SF_Cylinder segment({ 0., 0., 0. }, { 0., 0., 1. }, 1.);
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    3 * std::numbers::pi / 2., Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ -0.5, 0., -0. };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(CylinderTest, RotationModifiesCenterY3)
{
  SF_Cylinder segment({ 0., 0., 0. }, { 0., 0., 1. }, 1.);
  Eigen::Affine3d rotationAroundY90(
    Eigen::AngleAxis<double>(std::numbers::pi, Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0., 0., -0.5 };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(CylinderTest, RotationModifiesCenterX)
{
  SF_Cylinder segment({ 0., 0., 0. }, { 0., 0., 1. }, 1.);
  Eigen::Affine3d rotationAroundY90(
    Eigen::AngleAxis<double>(std::numbers::pi, Eigen::Vector3d(1., 0., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0., 0., -0.5 };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(CylinderTest, ScalingScalarModifiesLength)
{
  SF_Cylinder segment({ 1., -7., 6. }, { -2., 3., 5. }, 1.);
  const auto lengthBefore = segment.Length();
  Eigen::Affine3d scalingByTwo(Eigen::Scaling(2.));
  segment.Transform(scalingByTwo);
  const auto lengthAfter = segment.Length();
  EXPECT_DOUBLE_EQ(2 * lengthBefore, lengthAfter);
}

TEST(CylinderTest, ScalingScalarPreservesRadius)
{
  SF_Cylinder segment({ 1., -7., 6. }, { -2., 3., 5. }, 1.);
  const auto radiusBefore = segment.Radius();
  Eigen::Affine3d scalingByTwo(Eigen::Scaling(2.));
  segment.Transform(scalingByTwo);
  const auto radiusAfter = segment.Radius();
  EXPECT_DOUBLE_EQ(radiusBefore, radiusAfter);
}

TEST(CylinderTest, ScalingScalarModifiesLengthX)
{
  SF_Cylinder segment({ -1., -2., -3. }, { 1., 2., 3. }, 1.);
  Eigen::Affine3d scalingByTwoX(Eigen::Scaling(2., 1., 1.));
  segment.Transform(scalingByTwoX);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(16. + 16. + 36.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}

TEST(CylinderTest, ScalingScalarModifiesLengthY)
{
  SF_Cylinder segment({ -1., -2., -3. }, { 1., 2., 3. }, 1.);
  Eigen::Affine3d scalingByTwoY(Eigen::Scaling(1., 2., 1.));
  segment.Transform(scalingByTwoY);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(4. + 64. + 36.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}

TEST(CylinderTest, ScalingScalarModifiesLengthZ)
{
  SF_Cylinder segment({ -1., -2., -3. }, { 1., 2., 3. }, 1.);
  Eigen::Affine3d scalingByTwoZ(Eigen::Scaling(1., 1., 2.));
  segment.Transform(scalingByTwoZ);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(4. + 16. + 144.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}
} // namespace geometry
} // namespace sf
