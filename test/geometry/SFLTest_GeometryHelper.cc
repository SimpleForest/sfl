#include <gtest/gtest.h>

#include "SF_Cylinder.h"
#include "SF_GeometryHelper.h"
#include "SF_Header.h"
#include "SF_Helper.h"
#include "SF_Line.h"
#include "SF_LineSegment.h"

namespace sf {

TEST(GeometryHelperTest, PointToPointDistanceIsCorrect)
{
  const Eigen::Vector3d first(-1., -1., -1.);
  const Eigen::Vector3d second(1., 1., 1.);
  const auto distance = geometry::helper::DistanceBetween(first, second);
  const auto expected = std::sqrt(12.0);
  EXPECT_EQ(expected, distance);
}

TEST(GeometryHelperTest, PointToPointDistanceIsCorrectIfEqual)
{
  const Eigen::Vector3d first(-1., -1., -1.);
  const auto distance = geometry::helper::DistanceBetween(first, first);
  const auto expected = 0.;
  EXPECT_EQ(expected, distance);
}

TEST(GeometryHelperTest, AngleBetweenIsCorrect)
{
  const Eigen::Vector3d first(2., 2., 0.);
  const Eigen::Vector3d second(0., 3., 0.);
  const auto angle = geometry::helper::AngleInRadBetween(first, second);
  const auto expected = std::numbers::pi / 4.;
  EXPECT_DOUBLE_EQ(expected, angle);
}

TEST(GeometryHelperTest, AngleBetweenIsCorrectIfEqual)
{
  const Eigen::Vector3d first(-1., -1., -1.);
  const auto angle = geometry::helper::AngleInRadBetween(first, first);
  const auto expected = 0.;
  EXPECT_DOUBLE_EQ(expected, angle);
}

TEST(GeometryHelperTest, DegToRadWorks)
{
  const auto rad = geometry::helper::DegToRad(45.);
  const auto expected = std::numbers::pi / 4.;
  EXPECT_DOUBLE_EQ(expected, rad);
}

TEST(GeometryHelperTest, RadToDegWorks)
{
  const auto deg = geometry::helper::RadToDeg(-3. * std::numbers::pi / 4.);
  const auto expected = -135.;
  EXPECT_DOUBLE_EQ(expected, deg);
}

TEST(GeometryHelperTest, AngleBetweenInDegIsCorrect)
{
  const Eigen::Vector3d first(2., 2., 0.);
  const Eigen::Vector3d second(0., 3., 0.);
  const auto angle = geometry::helper::AngleInDegBetween(first, second);
  const auto expected = 45.;
  EXPECT_DOUBLE_EQ(expected, angle);
}

TEST(GeometryHelperTest, SerializeHeaderForLinesWorks)
{
  geometry::SF_Line line({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 });
  SF_Header header(line);
  std::stringstream stream;
  stream << header;
  const auto actual = "firstX, firstY, firstZ, secondX, secondY, secondZ";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(GeometryHelperTest, SerializeHeaderForLineSegmentsWorks)
{
  geometry::SF_LineSegment lineSegment({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 });
  SF_Header header(lineSegment);
  std::stringstream stream;
  stream << header;
  const auto actual = "startX, startY, startZ, endX, endY, endZ";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(GeometryHelperTest, SerializeHeaderForCylindersWorks)
{
  geometry::SF_Cylinder cylinder({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 }, 7.1);
  SF_Header header(cylinder);
  std::stringstream stream;
  stream << header;
  const auto actual = "startX, startY, startZ, endX, endY, endZ, radius";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(GeometryHelperTest, SerializeForLinesWorks)
{
  geometry::SF_Line line({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 });
  std::stringstream stream;
  stream << line;
  const auto actual = "1.1000, 2.1000, 3.1000, 4.1000, 5.1000, 6.1000";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(GeometryHelperTest, SerializeForLineSegmentsWorks)
{
  geometry::SF_LineSegment line({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 });
  std::stringstream stream;
  stream << line;
  const auto actual = "1.1000, 2.1000, 3.1000, 4.1000, 5.1000, 6.1000";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(GeometryHelperTest, SerializeForCylindersWorks)
{
  geometry::SF_Cylinder cylinder({ 1.1, 2.1, 3.1 }, { 4.1, 5.1, 6.1 }, 7.12343);
  std::stringstream stream;
  stream << cylinder;
  const auto actual = "1.1000, 2.1000, 3.1000, 4.1000, 5.1000, 6.1000, 7.1234";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

} // namespace sf
