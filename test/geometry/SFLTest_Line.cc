#include <gtest/gtest.h>

#include "SF_Line.h"

namespace sf {
namespace geometry {

class LineTest : public testing::Test
{
public:
  const Eigen::Vector3d ProjectionTest(const Eigen::Vector3d& point,
                                       const SF_Line& line)
  {
    return line.Projection(point);
  }
};

TEST_F(LineTest, PointToLineDistanceIsCorrect)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(3., 1., 1.);
  const auto distance = line.DistanceTo(point);
  const auto expected = std::sqrt(2.0);
  EXPECT_EQ(expected, distance);
}

TEST_F(LineTest, ProjectionIsCorrectIfColinear)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 1., 1. });
  const Eigen::Vector3d point(2., 2., 2.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(2., 2., 2.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrectX)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(-3., 3., 3.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(-3., 0., 0.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrectY)
{
  const SF_Line line({ 0., 0., 0. }, { 0., 1., 0. });
  const Eigen::Vector3d point(1., 0., 1.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(0., 0., 0.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrectZ)
{
  const SF_Line line({ 0., 0., 0. }, { 0., 0., 1. });
  const Eigen::Vector3d point(1., 0., 55.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(0., 0., 55.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrect2d)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 1., 0. });
  const Eigen::Vector3d point(1., 0., 0.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(1. / 2., 1. / 2., 0.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrect)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 1., 1. });
  const Eigen::Vector3d point(1., 0., 0.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(1. / 3., 1. / 3., 1. / 3.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrect2)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 1., 1. });
  const Eigen::Vector3d point(1., 0., 1.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(2. / 3., 2. / 3., 2. / 3.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

TEST_F(LineTest, ProjectionIsCorrect3)
{
  const SF_Line line({ 0., 0., 0. }, { 1., 1., 1. });
  const Eigen::Vector3d point(1., 1., 0.);
  const auto projection = ProjectionTest(point, line);
  const auto expected = Eigen::Vector3d(2. / 3., 2. / 3., 2. / 3.);
  EXPECT_DOUBLE_EQ(expected[0], projection[0]);
  EXPECT_DOUBLE_EQ(expected[1], projection[1]);
  EXPECT_DOUBLE_EQ(expected[2], projection[2]);
}

} // namespace geometry
} // namespace sf
