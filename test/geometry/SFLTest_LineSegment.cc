#include <gtest/gtest.h>

#include "SF_GeometryHelper.h"
#include "SF_LineSegment.h"

namespace sf {
namespace geometry {

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectX)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(3., 0., 0.);
  const auto distance = line.DistanceTo(point);
  const auto expected = 2.;
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectY)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 0., 1., 0. });
  const Eigen::Vector3d point(0., -10., 0.);
  const auto distance = line.DistanceTo(point);
  const auto expected = 10.;
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectZ)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 0., 0., 1. });
  const Eigen::Vector3d point(0., 0., 0.5);
  const auto distance = line.DistanceTo(point);
  const auto expected = 0.;
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectLeftOf)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(-1., 2., 2.);
  const auto distance = line.DistanceTo(point);
  const auto expected = std::sqrt(9.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectInBetween)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(0.5, 1., 1.);
  const auto distance = line.DistanceTo(point);
  const auto expected = std::sqrt(2.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, PointToLineSegmentDistanceIsCorrectRightOf)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 1., 0., 0. });
  const Eigen::Vector3d point(3., 1., 1.);
  const auto distance = line.DistanceTo(point);
  const auto expected = std::sqrt(6.);
  EXPECT_DOUBLE_EQ(expected, distance);
}

TEST(LineSegmentTest, BoundingRadiusIsCorrect)
{
  const SF_LineSegment line({ 0., 0., 0. }, { 3., 0., 0. });
  const auto boundingRadius = line.BoundingRadius();
  const auto expected = 1.5;
  EXPECT_DOUBLE_EQ(expected, boundingRadius);
}

TEST(LineSegmentTest, RotationPreservesLength)
{
  SF_LineSegment segment({ -1., -5., 17. }, { 3., 2., -13. });
  const auto lengthBefore = segment.Length();
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    std::numbers::pi * 0.91, Eigen::Vector3d(5., 1., 3.).normalized()));
  segment.Transform(rotationAroundY90);
  const auto lengthAfter = segment.Length();
  EXPECT_DOUBLE_EQ(lengthBefore, lengthAfter);
}

TEST(LineSegmentTest, RotationModifiesCenterY1)
{
  SF_LineSegment segment({ 0., 0., 0. }, { 0., 0., 1. });
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    std::numbers::pi / 2., Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0.5, 0., -0. };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(LineSegmentTest, RotationModifiesCenterY2)
{
  SF_LineSegment segment({ 0., 0., 0. }, { 0., 0., 1. });
  Eigen::Affine3d rotationAroundY90(Eigen::AngleAxis<double>(
    3 * std::numbers::pi / 2., Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ -0.5, 0., -0. };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(LineSegmentTest, RotationModifiesCenterY3)
{
  SF_LineSegment segment({ 0., 0., 0. }, { 0., 0., 1. });
  Eigen::Affine3d rotationAroundY90(
    Eigen::AngleAxis<double>(std::numbers::pi, Eigen::Vector3d(0., 1., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0., 0., -0.5 };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(LineSegmentTest, RotationModifiesCenterX)
{
  SF_LineSegment segment({ 0., 0., 0. }, { 0., 0., 1. });
  Eigen::Affine3d rotationAroundY90(
    Eigen::AngleAxis<double>(std::numbers::pi, Eigen::Vector3d(1., 0., 0.)));
  segment.Transform(rotationAroundY90);
  const auto center = segment.Center();
  Eigen::Vector3d expectedCenter{ 0., 0., -0.5 };
  EXPECT_NEAR(expectedCenter[0], center[0], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[1], center[1], helper::MAX_ERROR);
  EXPECT_NEAR(expectedCenter[2], center[2], helper::MAX_ERROR);
}

TEST(LineSegmentTest, ScalingScalarModifiesLength)
{
  SF_LineSegment segment({ 1., -7., 6. }, { -2., 3., 5. });
  const auto lengthBefore = segment.Length();
  Eigen::Affine3d scalingByTwo(Eigen::Scaling(2.));
  segment.Transform(scalingByTwo);
  const auto lengthAfter = segment.Length();
  EXPECT_DOUBLE_EQ(2 * lengthBefore, lengthAfter);
}

TEST(LineSegmentTest, ScalingScalarModifiesLengthX)
{
  SF_LineSegment segment({ -1., -2., -3. }, { 1., 2., 3. });
  Eigen::Affine3d scalingByTwoX(Eigen::Scaling(2., 1., 1.));
  segment.Transform(scalingByTwoX);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(16. + 16. + 36.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}

TEST(LineSegmentTest, ScalingScalarModifiesLengthY)
{
  SF_LineSegment segment({ -1., -2., -3. }, { 1., 2., 3. });
  Eigen::Affine3d scalingByTwoY(Eigen::Scaling(1., 2., 1.));
  segment.Transform(scalingByTwoY);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(4. + 64. + 36.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}

TEST(LineSegmentTest, ScalingScalarModifiesLengthZ)
{
  SF_LineSegment segment({ -1., -2., -3. }, { 1., 2., 3. });
  Eigen::Affine3d scalingByTwoZ(Eigen::Scaling(1., 1., 2.));
  segment.Transform(scalingByTwoZ);
  const auto lengthAfter = segment.Length();
  const auto expected = std::sqrt(4. + 16. + 144.);
  EXPECT_DOUBLE_EQ(expected, lengthAfter);
}

} // namespace geometry
} // namespace sf
