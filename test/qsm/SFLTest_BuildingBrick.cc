#include <algorithm>
#include <gtest/gtest.h>

#include <numbers>
#include <ranges>

#include "SF_BuildingBrick.h"
#include "SF_Cylinder.h"

namespace sf {
namespace qsm {

namespace {

std::shared_ptr<geometry::SF_Cylinder>
Geometry(double x)
{
  const Eigen::Vector3d start{ x, x, x };
  const Eigen::Vector3d end{ x + 1., x + 1., x + 1. };
  return std::make_shared<geometry::SF_Cylinder>(start, end, x + 2.);
}

std::shared_ptr<SF_BuildingBrick>
BuildingBrick(double x)
{
  std::shared_ptr<geometry::SF_Cylinder> cylinder = Geometry(x);
  return std::make_shared<SF_BuildingBrick>(cylinder,
                                            static_cast<std::uint32_t>(x));
}

} // namespace

TEST(BrickTest, OperatorPlusEqualsWorks)
{
  std::shared_ptr<SF_BuildingBrick> brick1 = BuildingBrick(1.);
  std::stringstream stream1a;
  brick1->Serialize(stream1a);
  std::shared_ptr<SF_BuildingBrick> brick2 = BuildingBrick(2.);
  std::stringstream stream2a;
  brick2->Serialize(stream2a);
  *brick1 = std::move(*brick2);
  std::stringstream stream1b;
  brick1->Serialize(stream1b);
  auto str1a = stream1a.str();
  auto str1b = stream1b.str();
  auto str2a = stream2a.str();
  EXPECT_EQ(str2a, str1b);
  EXPECT_NE(str1a, str1b);
}

TEST(BrickTest, EqualOperatorWorks1)
{
  std::shared_ptr<SF_BuildingBrick> brick1 = BuildingBrick(1.);
  std::shared_ptr<SF_BuildingBrick> brick2 = BuildingBrick(2.);
  EXPECT_EQ(*brick2, *brick2);
  EXPECT_EQ(*brick1, *brick1);
  EXPECT_NE(*brick1, *brick2);
  EXPECT_NE(*brick2, *brick1);
}

TEST(BrickTest, EqualOperatorWorks2)
{
  std::shared_ptr<SF_BuildingBrick> brick1 = BuildingBrick(1.);
  std::shared_ptr<SF_BuildingBrick> brick2 = BuildingBrick(2.);
  EXPECT_NE(*brick1, *brick2);
  brick2->SetId(1);
  EXPECT_NE(*brick1, *brick2);
  brick2->SetGeometry(Geometry(1.));
  EXPECT_EQ(*brick1, *brick2);
  brick2->SetId(2);
  EXPECT_NE(*brick1, *brick2);
}

TEST(BrickTest, EqualOperatorWorks3)
{
  std::shared_ptr<SF_BuildingBrick> brick1 = BuildingBrick(0.);
  std::shared_ptr<SF_BuildingBrick> brick2 = BuildingBrick(0.);
  EXPECT_NE(brick1, brick2);
  EXPECT_EQ(*brick1, *brick2);
  (*brick1) += brick2;
  EXPECT_NE(*brick1, *brick2);
}

TEST(BrickTest, EqualOperatorWorksRecursively)
{
  std::shared_ptr<SF_BuildingBrick> brick0 = BuildingBrick(0.);
  std::shared_ptr<SF_BuildingBrick> brick1 = BuildingBrick(1.);
  (*brick0) += brick1;
  auto node0Copy = brick0->DeepCopy();
  EXPECT_NE(brick0, node0Copy);
  EXPECT_EQ(*brick0, *node0Copy);
  std::shared_ptr<SF_BuildingBrick> node2 = BuildingBrick(2.);
  (*brick1) += node2;
  EXPECT_NE(*brick0, *node0Copy);
  (*node0Copy->Children().front()) += node2;
  EXPECT_EQ(*brick0, *node0Copy);
  brick0->Children().front()->Children().front()->SetId(
    123); // both trees node2!
  EXPECT_EQ(*brick0, *node0Copy);
  brick0->Children().front()->SetId(123);
  EXPECT_NE(*brick0, *node0Copy);
}

} // namespace topology
} // namespace sf
