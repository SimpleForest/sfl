#include <algorithm>
#include <gtest/gtest.h>

#include <numbers>
#include <ranges>

#include "SF_Node.h"

namespace sf {
namespace topology {

namespace {

constexpr std::uint32_t size = 30u;

std::shared_ptr<SF_Node>
LinearNode()
{
  std::shared_ptr<SF_Node> root = std::make_shared<SF_Node>(SF_Node(0));
  auto current = root;
  for (std::uint32_t i = 1; i < size; ++i) {
    std::shared_ptr<SF_Node> child = std::make_shared<SF_Node>(SF_Node(i));
    (*current) += child;
    current = std::move(child);
  }
  return root;
}

void
SplitNode(std::shared_ptr<SF_Node> node, std::int32_t& id)
{
  for (std::uint32_t i = 0; i < 3; ++i) {
    if (id >= size) {
      return;
    }
    std::shared_ptr<SF_Node> child = std::make_shared<SF_Node>(SF_Node(id++));
    (*node) += child;
  }
  for (const auto& child : node->Children()) {
    SplitNode(child, id);
  }
}

std::shared_ptr<SF_Node>
SplitNode()
{
  std::int32_t id{ 0 };
  std::shared_ptr<SF_Node> root = std::make_shared<SF_Node>(SF_Node(id));
  SplitNode(root, id);
  return root;
}

} // namespace

TEST(NodeTest, OperatorPlusEqualsWorks)
{
  std::shared_ptr<SF_Node> root = std::make_shared<SF_Node>(SF_Node(0));
  std::shared_ptr<SF_Node> child = std::make_shared<SF_Node>(SF_Node(1));
  (*root) += child;
  EXPECT_EQ(1, root->Children().size());
  EXPECT_EQ(child, root->Children().front());
}

TEST(NodeTest, EqualOperatorWorks1)
{
  const SF_Node node0(0);
  const SF_Node node1(1);
  EXPECT_EQ(node0, node0);
  EXPECT_NE(node0, node1);
}

TEST(NodeTest, EqualOperatorWorks2)
{
  const SF_Node node0(0);
  SF_Node node1(1);
  EXPECT_NE(node0, node1);
  node1.SetId(0);
  EXPECT_EQ(node0, node1);
}

TEST(NodeTest, EqualOperatorWorks3)
{
  std::shared_ptr<SF_Node> node0 = std::make_shared<SF_Node>(SF_Node(0));
  std::shared_ptr<SF_Node> node1 = std::make_shared<SF_Node>(SF_Node(0));
  EXPECT_NE(node0, node1);
  EXPECT_EQ(*node0, *node1);
  (*node0) += node1;
  EXPECT_NE(node0, node1);
}

TEST(NodeTest, EqualOperatorWorksRecursively)
{
  auto node0 = std::make_shared<SF_Node>(SF_Node(0));
  auto node1 = std::make_shared<SF_Node>(SF_Node(1));
  (*node0) += node1;
  auto node0Copy = node0->DeepCopy();
  EXPECT_NE(node0, node0Copy);
  EXPECT_EQ(*node0, *node0Copy);
  std::shared_ptr<SF_Node> node2 = std::make_shared<SF_Node>(SF_Node(2));
  (*node1) += node2;
  EXPECT_NE(*node0, *node0Copy);
}

TEST(NodeTest, IdsAreCorrectLinear)
{
  auto root = LinearNode();
  std::vector<std::shared_ptr<SF_Node>> childrenRoot =
    root->ChildrenRecursively();
  EXPECT_EQ(size, childrenRoot.size());
  std::vector<std::int32_t> ids;
  std::transform(childrenRoot.cbegin(),
                 childrenRoot.cend(),
                 std::back_inserter(ids),
                 [](const auto& node) { return node->Id(); });
  std::vector<std::int32_t> thirtyToOne = std::ranges::iota_view(0, 30) |
                                          std::views::reverse |
                                          std::ranges::to<std::vector>();
  EXPECT_EQ(thirtyToOne, ids);
}

TEST(NodeTest, ParentIdsAreCorrectLinear)
{
  auto root = LinearNode();
  std::vector<std::shared_ptr<SF_Node>> childrenRoot =
    root->ChildrenRecursively();
  EXPECT_EQ(size, childrenRoot.size());
  std::vector<std::int32_t> parentIds;
  std::ranges::transform(childrenRoot,
                         std::back_inserter(parentIds),
                         [](const auto& node) { return node->ParentId(); });
  auto twentyNineToZero = std::ranges::iota_view(-1, 29) | std::views::reverse |
                          std::ranges::to<std::vector>();
  EXPECT_EQ(twentyNineToZero, parentIds);
}

TEST(NodeTest, ChildrenAreAlwaysRetrievedBeforeParents)
{
  auto root = SplitNode();
  std::vector<std::shared_ptr<SF_Node>> nodes = root->ChildrenRecursively();
  std::int32_t id{ 0 };
  for (const auto& node : nodes) {
    node->SetId(id++);
  }
  for (const auto& node : nodes) {
    for (const auto& child : node->Children()) {
      EXPECT_GT(node->Id(), child->Id());
    }
  }
}

} // namespace topology
} // namespace sf
