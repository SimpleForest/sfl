#include <gtest/gtest.h>

#include "SF_Header.h"
#include "SF_Node.h"

#include "SF_Helper.h"
#include "SF_TopologyHelper.h"

namespace sf {

TEST(TopologyHelperTest, SerializeHeaderForNodesWorks)
{
  std::shared_ptr<topology::SF_Node> parent =
    std::make_shared<topology::SF_Node>(0);
  std::shared_ptr<topology::SF_Node> childA =
    std::make_shared<topology::SF_Node>(1);
  std::shared_ptr<topology::SF_Node> grandchild =
    std::make_shared<topology::SF_Node>(2);
  std::shared_ptr<topology::SF_Node> childB =
    std::make_shared<topology::SF_Node>(3);
  *parent += childA;
  *childA += grandchild;
  *parent += childB;

  SF_Header header(*parent);
  std::stringstream stream;
  stream << header;
  const auto actual = "Id, ParentId";
  const auto expected = stream.str();
  EXPECT_EQ(expected, actual);
}

TEST(TopologyHelperTest, SerializeForNodesWorks)
{
  std::shared_ptr<topology::SF_Node> parent =
    std::make_shared<topology::SF_Node>(0);
  std::shared_ptr<topology::SF_Node> childA =
    std::make_shared<topology::SF_Node>(1);
  std::shared_ptr<topology::SF_Node> grandchild =
    std::make_shared<topology::SF_Node>(2);
  std::shared_ptr<topology::SF_Node> childB =
    std::make_shared<topology::SF_Node>(3);
  *parent += childA;
  *childA += grandchild;
  *parent += childB;
  {
    std::stringstream stream;
    stream << *parent;
    const auto expected = "0, -1";
    const auto actual = stream.str();
    EXPECT_EQ(expected, actual);
  }
  {
    std::stringstream stream;
    stream << *childA;
    const auto expected = "1, 0";
    const auto actual = stream.str();
    EXPECT_EQ(expected, actual);
  }
  {
    std::stringstream stream;
    stream << *grandchild;
    const auto expected = "2, 1";
    const auto actual = stream.str();
    EXPECT_EQ(expected, actual);
  }
  {
    std::stringstream stream;
    stream << *childB;
    const auto expected = "3, 0";
    const auto actual = stream.str();
    EXPECT_EQ(expected, actual);
  }
}

} // namespace sf
